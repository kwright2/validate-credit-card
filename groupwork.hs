
--Question 0:

lastDigit :: Integer -> Integer
lastDigit x = mod x 10

--Question 1: 

dropLastDigit :: Integer -> Integer
dropLastDigit x = x `div` 10

--Question 2:
toDigits :: Integer -> [Integer]
 
toDigits 0 = []
toDigits x =  (toDigits (dropLastDigit x)) ++ [(lastDigit x)]


--Question 3: 

reverseList :: [Integer] -> [Integer]

reverseList [] = []
reverseList (x:xs) = reverseList xs ++ [x]


doubleEveryOther :: [Integer] -> [Integer]
doubleEveryOther x = reverseList(doubleHelper (reverseList x) )

doubleHelper :: [Integer] -> [Integer]
doubleHelper [] = []
doubleHelper (x: []) = [x]
doubleHelper (x:b:xs) =  [x] ++ [(2*b)] ++ (doubleHelper xs)

--Question 4

sumDigits :: [Integer] -> Integer
sumDigits (x:[]) = (lastDigit x) + (dropLastDigit x)
sumDigits (x:xs) = (lastDigit x)  + (dropLastDigit x) + (sumDigits xs)

--Question 5
validateCard :: Integer -> Bool

validateCard x 
	| (sumDigits (doubleEveryOther (toDigits x))) `mod` 10 == 0 = True
	| otherwise = False


 --Extra Credit 
main :: IO ()
main = do
	putStrLn "Insert Card Number"
	number <- getLine
	putStrLn ("Your Card Is Valid:" ++ (show (validateCard (read number :: Integer))))
 